﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Xpos.ViewModels;
using XposWeb.AddOn;
using XposWeb.Models;

namespace XposWeb.Controllers
{
    public class ProductsController : Controller
    {
        private VariantsModel variant;
        private CategoriesModel category;
        private ProductsModel product;
        private readonly string imgFolder;
        private readonly int pageSize;

        private readonly IWebHostEnvironment webHostEnv;
        public ProductsController(IConfiguration _config, IWebHostEnvironment _webHostEnv)
        {
            variant = new VariantsModel(_config["ApiMasterUrl"]);
            category = new CategoriesModel(_config["ApiMasterUrl"]);
            product = new ProductsModel(_config["ApiMasterUrl"], _webHostEnv);
            imgFolder = _config["ImagesFolder"];
            pageSize = int.Parse(_config["PageSize"]);
            
        }
        public IActionResult Index(string? filter, string? orderBy, int? currPageSize, int? pageNumber)
        {
            int totalData = 0;
            List<VMTblMProduct>? data;
            if (string.IsNullOrEmpty(filter))
            {
                List<object>? pagingData = product.GetAllPaging(pageNumber??1,currPageSize??pageSize);
                data = JsonConvert.DeserializeObject<List<VMTblMProduct>?>(JsonConvert.SerializeObject(pagingData[0]));
                totalData = JsonConvert.DeserializeObject<int>(JsonConvert.SerializeObject(pagingData[1]));
            }
            else
            {
                data = product.GetByFilter(filter);
            }

            switch (orderBy)
            {
                case "stock_desc":
                    data = data.OrderByDescending(p => p.Stock).ToList();
                    break;
                case "stock":
                    data = data.OrderBy(p => p.Stock).ToList();
                    break;
                case "price_desc":
                    data = data.OrderByDescending(p => p.Price).ToList();
                    break;
                case "price":
                    data = data.OrderBy(p => p.Price).ToList();
                    break;
                case "category_desc":
                    data = data.OrderByDescending(p => p.CategoryName).ToList();
                    break;
                case "category":
                    data = data.OrderBy(p => p.CategoryName).ToList();
                    break;
                case "variant_desc":
                    data = data.OrderByDescending(p => p.VariantName).ToList();
                    break;
                case "variant":
                    data = data.OrderBy(p => p.VariantName).ToList();
                    break;
                case "name_desc":
                    data = data.OrderByDescending(p => p.Name).ToList();
                    break;
                case "name":
                    data = data.OrderBy(p => p.Name).ToList();
                    break;
                case "id_desc":
                    data = data.OrderByDescending(p => p.Id).ToList();
                    break;
                default:
                    data = data.OrderBy(p => p.Id).ToList();
                    break;
            }

            ViewBag.Title = "Product List";
            ViewBag.ImgFolder = imgFolder;
            //current page parameter
            ViewBag.Filter = filter;
            ViewBag.Order = orderBy;
            ViewBag.PageSize = currPageSize ?? pageSize;
            //update order selection
            ViewBag.OrderId = string.IsNullOrEmpty(orderBy) ? "id_desc":"";   
            ViewBag.OrderName = orderBy == "nama"?"name_desc":"name";
            ViewBag.OrderVariant = orderBy == "variant"?"variant_desc":"variant";
            ViewBag.OrderCategory = orderBy == "category"?"category_desc":"category";
            ViewBag.OrderPrice = orderBy == "price"?"price_desc":"price";
            ViewBag.OrderStock = orderBy == "stock"?"stock_desc":"stock";

            return View(Pagination<VMTblMProduct>.Create(data, totalData, pageNumber ?? 1, ViewBag.PageSize));
        }

        public IActionResult Details(int id)
        {
            ViewBag.Title = "Product Details";
            ViewBag.ImgFolder = imgFolder;
            VMTblMProduct? data = product.GetById(id);
            return View(data);
        }

        public IActionResult Create()
        {
            ViewBag.Title = "New Product";
            //ViewBag.catData = category.GetAll();
            //ViewBag.varData = variant.GetAll();
            ViewBag.Categories = category.GetAll().Select(
                c => new SelectListItem
                {
                    Value = c.Id.ToString(),
                    Text = c.CategoryName
                }
                ).ToList();
            ViewBag.Variants = variant.GetAll();
            return View();
        }

        [HttpPost]
        public VMTblMProduct? Add(VMTblMProduct data)
        {
            //upload if image existed  
            try
            {
                if (data.imageFile != null)
                {
                    //upload image process
                    data.Image = product.UploadImage(imgFolder, data.imageFile);
                    data.imageFile = null;
                }
                return product.Create(data);
            }
            catch(Exception ex)
            {
                //logging
                Console.WriteLine(ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public IActionResult Edit(int id)
        {
            ViewBag.Title = "Product Update";
            ViewBag.CatData = category.GetAll().Select(
              c => new SelectListItem
              {
                  Value = c.Id.ToString(),
                  Text = c.CategoryName
              }
              ).ToList();
            ViewBag.VarData = variant.GetAll();
            VMTblMProduct? data = product.GetById(id);
            return View(data);
        }

        [HttpPost]
        public VMTblMProduct? Update(VMTblMProduct data)
        {
            try
            {
                if(data.imageFile != null)
                {
                    if(data.Image != null)
                    {
                        product.DeleteOldImage(imgFolder, data.Image);
                    }
                    data.Image = product.UploadImage(imgFolder, data.imageFile);
                    data.imageFile = null;
                }
                return product.Edit(data);
            }
            catch(Exception ex)
            {
                //logging
                Console.WriteLine(ex.Message);
                throw new Exception(ex.Message);
            }
        }
        
        
        public IActionResult Delete(int id)
        {
            ViewBag.Title = "Delete Product";
            return View(id);
        }

        //[HttpPost]

        //public VMTblMProduct? Delete(VMTblMProduct data)
        //{
        //    VMTblMProduct? respon = product.Delete(data.Id, (int)data.UpdateBy!);

        //    if(respon != null)
        //    {
        //        if(data.Image != null)
        //        {
        //            product.Delete();
        //        }
        //    }
        //}
    }
}
