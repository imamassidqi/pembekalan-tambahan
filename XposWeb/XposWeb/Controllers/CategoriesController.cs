﻿using Microsoft.AspNetCore.Mvc;
using Xpos.ViewModels;
using XposWeb.Models;

namespace XposWeb.Controllers
{
    public class CategoriesController : Controller
    {
        private CategoriesModel category;

        public CategoriesController(IConfiguration _config)
        {
            category = new CategoriesModel(_config["ApiMasterUrl"]);
        }
        public IActionResult Index(string filter)
        {
            List<VMTblMCategory>? data;
            if (string.IsNullOrEmpty(filter))
            {
                data = category.GetAll();
            }
            else
            {
                data = category.GetByFilter(filter);
            }
            ViewBag.Title = "Category List";
            ViewBag.Filter = filter;
            return View(data);
        }

        public IActionResult Details(int id)
        {
            ViewBag.Title = "Category Details";
            VMTblMCategory? data = category.GetById(id);
            return View(data);
        }

        public IActionResult Create()
        {
            ViewBag.Title = "New Category";

            return View();
        }

        [HttpPost]
        public VMTblMCategory? Add(VMTblMCategory data)
        {
            VMTblMCategory? newData = category.Create(data);
            return newData;
        }

        public IActionResult Edit(int id)
        {
            ViewBag.Title = "Category Update";
            VMTblMCategory? data = category.GetById(id);
            return View(data);
        }

        [HttpPost]
        public VMTblMCategory? Update(VMTblMCategory data) => category.Edit(data);

        public IActionResult Delete(int id)
        {
            ViewBag.Title = "Delete Category";
            return View(id);
        }

        [HttpPost]

        public VMTblMCategory? Delete(VMTblMCategory data) => category.Delete(data.Id, (int)data.UpdateBy!);
    }
}
