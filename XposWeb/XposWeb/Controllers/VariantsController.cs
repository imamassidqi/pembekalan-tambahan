﻿using Microsoft.AspNetCore.Mvc;
using Xpos.ViewModels;
using XposWeb.Models;

namespace XposWeb.Controllers
{
    public class VariantsController : Controller
    {
        private VariantsModel variant;
        private CategoriesModel category;

        public VariantsController(IConfiguration _config)
        {
            variant = new VariantsModel(_config["ApiMasterUrl"]);
            category = new CategoriesModel(_config["ApiMasterUrl"]);
        }
        public IActionResult Index(string filter)
        {
            List<VMTblMVariant>? data;
            if (string.IsNullOrEmpty(filter))
            {
                data = variant.GetAll();
            }
            else
            {
                data = variant.GetByFilter(filter);
            }
            ViewBag.Title = "Variant List";
            ViewBag.Filter = filter;
            return View(data);
        }

        public IActionResult Details(int id)
        {
            ViewBag.Title = "Variant Details";
            VMTblMVariant? data = variant.GetById(id);
            return View(data);
        }

        public IActionResult Create()
        {
            ViewBag.Title = "New Variant";
            ViewBag.catData = category.GetAll();
            return View();
        }

        [HttpPost]
        public VMTblMVariant? Add(VMTblMVariant data) => variant.Create(data);

        public IActionResult Edit(int id)
        {
            ViewBag.Title = "Variant Update";
            ViewBag.CatData = category.GetAll();
            VMTblMVariant? data = variant.GetById(id);
            return View(data);
        }

        [HttpPost]
        public VMTblMVariant? Update(VMTblMVariant data) => variant.Edit(data);

        public IActionResult Delete(int id)
        {
            ViewBag.Title = "Delete variant";
            return View(id);
        }

        [HttpPost]

        public VMTblMVariant? Delete(VMTblMVariant data) => variant.Delete(data.Id, (int)data.UpdateBy!);

    }
}
