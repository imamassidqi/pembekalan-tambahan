﻿using Microsoft.AspNetCore.Mvc;
using Xpos.ViewModels;
using XposWeb.Models;

namespace XposWeb.Controllers
{
    public class CustomersController : Controller
    {
        private CustomersModel customer;
        public CustomersController(IConfiguration _config)
        {
            customer = new CustomersModel(_config["ApiMasterUrl"]);
        }
        public IActionResult Index(string filter)
        {
            List<VMTblMCustomer>? data;
            if (string.IsNullOrEmpty(filter))
            {
                data = customer.GetAll();
            }
            else
            {
                data = customer.GetByFilter(filter);
            }
            ViewBag.Title = "Customer List";
            ViewBag.Filter = filter;
            return View(data);
        }

        public IActionResult Details(int id)
        {
            ViewBag.Title = "Customer Details";
            VMTblMCustomer? data = customer.GetById(id);
            return View(data);
        }

        public IActionResult Create()
        {
            ViewBag.Title = "New Category";

            return View();
        }

        [HttpPost]
        public VMTblMCustomer? Add(VMTblMCustomer data)
        {
            VMTblMCustomer? newData = customer.Create(data);
            return newData;
        }

        public IActionResult Edit(int id)
        {
            ViewBag.Title = "Customer Update";
            VMTblMCustomer? data = customer.GetById(id);
            return View(data);
        }

        [HttpPost]
        public VMTblMCustomer? Update(VMTblMCustomer data) => customer.Edit(data);


    }
}
