﻿using Newtonsoft.Json;
using System.Net;
using System.Text;
using Xpos.ViewModels;
namespace XposWeb.Models
{
    public class ProductsModel
    {
        private readonly string apiMaster;
        private readonly HttpClient httpClient = new HttpClient();
        private HttpContent content;
        private string jsonData;

        private readonly IWebHostEnvironment webHostEnv;
        public ProductsModel(string apiMasterUrl, IWebHostEnvironment _webHostEnv)
        {
            apiMaster = apiMasterUrl;
            webHostEnv = _webHostEnv;
        }

        public List<VMTblMProduct>? GetAll()
        {
            try
            {
                //API Call
                var apiResponse = httpClient.GetStringAsync($"{apiMaster}/Products").Result;
                return JsonConvert.DeserializeObject<List<VMTblMProduct>?>(apiResponse);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return null;
            }
        }

        public VMTblMProduct? GetById(int id)
        {
            try
            {
                //API Call
                var apiResponse = httpClient.GetStringAsync($"{apiMaster}/Products/Get/{id}").Result;
                return JsonConvert.DeserializeObject<VMTblMProduct?>(apiResponse);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return null;
            }
        }

        public VMTblMProduct? Create(VMTblMProduct data)
        {
            try
            {
                jsonData = JsonConvert.SerializeObject(data);
                content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                var apiResponse = httpClient.PostAsync($"{apiMaster}/Products", content).Result;
                if (apiResponse.StatusCode == HttpStatusCode.Created)
                {
                    return JsonConvert.DeserializeObject<VMTblMProduct?>(apiResponse.Content.ReadAsStringAsync().Result);
                }
                else
                {
                    throw new Exception(apiResponse.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{ex.Message}");
                return null;
            }
        }

        public string? UploadImage(string imageFolder, IFormFile? imageFile)
        {
            string uniquefilename = string.Empty;
            if (imageFile != null)
            {
                uniquefilename = Guid.NewGuid().ToString() + "-" + imageFile.FileName;

                string uploadFolder = webHostEnv.WebRootPath + "\\" + imageFolder + "\\" + uniquefilename;

                using (FileStream fileStream = new FileStream(uploadFolder, FileMode.Create))
                {
                    imageFile.CopyTo(fileStream);
                }
            }
            return uniquefilename;
        }

        public bool DeleteOldImage (string imageFolder, string oldfilename)
        {
            try
            {
                oldfilename = $"{webHostEnv.WebRootPath}/{imageFolder}/{oldfilename}";

                if (File.Exists(oldfilename))
                    File.Delete(oldfilename);
                else
                    throw new Exception("File does not exist");
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public VMTblMProduct? Edit(VMTblMProduct data)
        {
            try
            {
                jsonData = JsonConvert.SerializeObject(data);
                content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                var apiResponse = httpClient.PutAsync($"{apiMaster}/Products", content).Result;
                if (apiResponse.StatusCode == HttpStatusCode.OK)
                {
                    return JsonConvert.DeserializeObject<VMTblMProduct?>(apiResponse.Content.ReadAsStringAsync().Result);
                }
                else
                {
                    throw new Exception(apiResponse.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{ex.Message}");
                throw new Exception(ex.Message);

            }
        }

        public VMTblMProduct? Delete(int id, int userId)
        {
            try
            {
                //API Call
                var apiResponse = httpClient.DeleteAsync($"{apiMaster}/Products?id={id}&userId={userId}").Result;

                if (apiResponse.StatusCode == HttpStatusCode.OK)
                {
                    return JsonConvert.DeserializeObject<VMTblMProduct?>(apiResponse.Content.ReadAsStringAsync().Result);
                }
                else
                {
                    throw new Exception($"{(int)apiResponse.StatusCode}-{apiResponse.ReasonPhrase}");
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return null;
            }
        }

        public List<VMTblMProduct>? GetByFilter(string filter)
        {
            try
            {
                //API Call
                var apiResponse = httpClient.GetStringAsync($"{apiMaster}/Products/GetBy/{filter}").Result;
                return JsonConvert.DeserializeObject<List<VMTblMProduct>?>(apiResponse);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return null;
            }
        }

        public List<object>? GetAllPaging(int pageIndex, int pageSize)
        {
            try
            {
                //API Call
                var apiResponse = httpClient.GetStringAsync($"{apiMaster}/Products/GetByPaging/{pageIndex}/{pageSize}").Result;
                return JsonConvert.DeserializeObject<List<object>?>(apiResponse);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return null;
            }
        }
    }
}
