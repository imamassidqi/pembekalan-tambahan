﻿using Newtonsoft.Json;
using System.Net;
using System.Text;
using Xpos.ViewModels;

namespace XposWeb.Models
{
    public class VariantsModel
    {
        private readonly string apiMaster;
        private readonly HttpClient httpClient = new HttpClient();
        private HttpContent content;
        private string jsonData;
        public VariantsModel(string apiMasterUrl)
        {
            apiMaster = apiMasterUrl;
        }

        public List<VMTblMVariant>? GetAll()
        {
            try
            {
                //API Call
                var apiResponse = httpClient.GetStringAsync($"{apiMaster}/Variants").Result;
                return JsonConvert.DeserializeObject<List<VMTblMVariant>?>(apiResponse);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return null;
            }
        }

        public VMTblMVariant? GetById(int id)
        {
            try
            {
                //API Call
                var apiResponse = httpClient.GetStringAsync($"{apiMaster}/Variants/Get/{id}").Result;
                return JsonConvert.DeserializeObject<VMTblMVariant?>(apiResponse);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return null;
            }
        }

        public VMTblMVariant? Create(VMTblMVariant data)
        {
            try
            {
                jsonData = JsonConvert.SerializeObject(data);
                content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                var apiResponse = httpClient.PostAsync($"{apiMaster}/Variants", content).Result;
                if (apiResponse.StatusCode == HttpStatusCode.Created)
                {
                    return JsonConvert.DeserializeObject<VMTblMVariant?>(apiResponse.Content.ReadAsStringAsync().Result);
                }
                else
                {
                    throw new Exception(apiResponse.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{ex.Message}");
                throw new Exception(ex.Message);

            }
        }

        public VMTblMVariant? Edit(VMTblMVariant data)
        {
            try
            {
                jsonData = JsonConvert.SerializeObject(data);
                content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                var apiResponse = httpClient.PutAsync($"{apiMaster}/Variants", content).Result;
                if (apiResponse.StatusCode == HttpStatusCode.OK)
                {
                    return JsonConvert.DeserializeObject<VMTblMVariant?>(apiResponse.Content.ReadAsStringAsync().Result);
                }
                else
                {
                    throw new Exception(apiResponse.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{ex.Message}");
                throw new Exception(ex.Message);

            }
        }

        public VMTblMVariant? Delete(int id, int userId)
        {
            try
            {
                //API Call
                var apiResponse = httpClient.DeleteAsync($"{apiMaster}/Variants?id={id}&userId={userId}").Result;

                if (apiResponse.StatusCode == HttpStatusCode.OK)
                {
                    return JsonConvert.DeserializeObject<VMTblMVariant?>(apiResponse.Content.ReadAsStringAsync().Result);
                }
                else
                {
                    throw new Exception($"{(int)apiResponse.StatusCode}-{apiResponse.ReasonPhrase}");
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return null;
            }
        }

        public List<VMTblMVariant>? GetByFilter(string filter)
        {
            try
            {
                //API Call
                var apiResponse = httpClient.GetStringAsync($"{apiMaster}/Variants/GetBy/{filter}").Result;
                return JsonConvert.DeserializeObject<List<VMTblMVariant>?>(apiResponse);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return null;
            }
        }
    }
}
