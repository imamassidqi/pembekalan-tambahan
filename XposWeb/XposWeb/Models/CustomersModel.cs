﻿using Newtonsoft.Json;
using System.Net;
using System.Text;
using Xpos.ViewModels;

namespace XposWeb.Models
{
    public class CustomersModel
    {
        private readonly string apiMaster;
        private readonly HttpClient httpClient = new HttpClient();
        private HttpContent content;
        private string jsonData;

        public CustomersModel(string apiMasterUrl)
        {
            apiMaster = apiMasterUrl;
        }

        public List<VMTblMCustomer>? GetAll()
        {
            try
            {
                //API Call
                var apiResponse = httpClient.GetStringAsync($"{apiMaster}/Customers").Result;
                return JsonConvert.DeserializeObject<List<VMTblMCustomer>?>(apiResponse);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return null;
            }
        }

        public VMTblMCustomer? GetById(int id)
        {
            try
            {
                //API Call
                var apiResponse = httpClient.GetStringAsync($"{apiMaster}/Customers/Get/{id}").Result;
                return JsonConvert.DeserializeObject<VMTblMCustomer?>(apiResponse);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return null;
            }
        }

        public VMTblMCustomer? Create(VMTblMCustomer data)
        {
            try
            {
                jsonData = JsonConvert.SerializeObject(data);
                content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                var apiResponse = httpClient.PostAsync($"{apiMaster}/Customers", content).Result;
                if (apiResponse.StatusCode == HttpStatusCode.Created)
                {
                    return JsonConvert.DeserializeObject<VMTblMCustomer?>(apiResponse.Content.ReadAsStringAsync().Result);
                }
                else
                {
                    throw new Exception(apiResponse.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{ex.Message}");
                return null;
            }
        }

        public VMTblMCustomer? Edit(VMTblMCustomer data)
        {
            try
            {
                jsonData = JsonConvert.SerializeObject(data);
                content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                var apiResponse = httpClient.PutAsync($"{apiMaster}/Customers", content).Result;
                if (apiResponse.StatusCode == HttpStatusCode.OK)
                {
                    return JsonConvert.DeserializeObject<VMTblMCustomer?>(apiResponse.Content.ReadAsStringAsync().Result);
                }
                else
                {
                    throw new Exception(apiResponse.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{ex.Message}");
                throw new Exception(ex.Message);

            }
        }

        public List<VMTblMCustomer>? GetByFilter(string filter)
        {
            try
            {
                //API Call
                var apiResponse = httpClient.GetStringAsync($"{apiMaster}/Customers/GetBy/{filter}").Result;
                return JsonConvert.DeserializeObject<List<VMTblMCustomer>?>(apiResponse);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return null;
            }
        }
    }
}
