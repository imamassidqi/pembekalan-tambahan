﻿using Newtonsoft.Json;
using System.Net;
using System.Text;
using Xpos.ViewModels;

namespace XposWeb.Models
{
    public class CategoriesModel
    {
        private readonly string apiMaster;
        private readonly HttpClient httpClient = new HttpClient();
        private HttpContent content;
        private string jsonData;
        public CategoriesModel(string apiMasterUrl)
        {
            apiMaster = apiMasterUrl;   
        }

        public List<VMTblMCategory>? GetAll()
        {
            try
            {
                //API Call
                var apiResponse = httpClient.GetStringAsync($"{apiMaster}/Categories").Result;
                return JsonConvert.DeserializeObject<List<VMTblMCategory>?>(apiResponse);

            }
            catch (Exception ex) 
            {
                Console.WriteLine(ex.Message);

                return null;
            }
        }

        public VMTblMCategory? GetById(int id)
        {
            try
            {
                //API Call
                var apiResponse = httpClient.GetStringAsync($"{apiMaster}/Categories/Get/{id}").Result;
                return JsonConvert.DeserializeObject<VMTblMCategory?>(apiResponse);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return null;
            }
        }

        public VMTblMCategory? Create(VMTblMCategory data)
        {
            try
            {
                jsonData = JsonConvert.SerializeObject(data);
                content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                var apiResponse = httpClient.PostAsync($"{apiMaster}/Categories", content).Result;
                if (apiResponse.StatusCode == HttpStatusCode.Created)
                {
                    return JsonConvert.DeserializeObject<VMTblMCategory?>(apiResponse.Content.ReadAsStringAsync().Result);
                }
                else
                {
                    throw new Exception(apiResponse.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{ex.Message}");
                return null;
            }
        }

        public VMTblMCategory? Edit(VMTblMCategory data)
        {
            try
            {
                jsonData = JsonConvert.SerializeObject(data);
                content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                var apiResponse = httpClient.PutAsync($"{apiMaster}/Categories", content).Result;
                if (apiResponse.StatusCode == HttpStatusCode.OK)
                {
                    return JsonConvert.DeserializeObject<VMTblMCategory?>(apiResponse.Content.ReadAsStringAsync().Result);
                }
                else
                {
                    throw new Exception(apiResponse.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{ex.Message}");
                throw new Exception(ex.Message);

            }
        }

        public VMTblMCategory? Delete (int id, int userId)
        {
            try
            {
                //API Call
                var apiResponse = httpClient.DeleteAsync($"{apiMaster}/Categories?id={id}&userId={userId}").Result;
                
                if (apiResponse.StatusCode == HttpStatusCode.OK)
                {
                    return JsonConvert.DeserializeObject<VMTblMCategory?>(apiResponse.Content.ReadAsStringAsync().Result);
                }
                else
                {
                    throw new Exception($"{(int)apiResponse.StatusCode}-{apiResponse.ReasonPhrase}");
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return null;
            }
        }

        public List<VMTblMCategory>? GetByFilter(string filter)
        {
            try
            {
                //API Call
                var apiResponse = httpClient.GetStringAsync($"{apiMaster}/Categories/GetBy/{filter}").Result;
                return JsonConvert.DeserializeObject<List<VMTblMCategory>?>(apiResponse);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return null;
            }
        }
    }
}
