﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Xpos.ViewModels
{
    public partial class VMTblMCategory
    {
  
        public int Id { get; set; }
 
        public string CategoryName { get; set; } = null!;
 
        public string Description { get; set; } = null!;
  
        public bool? IsDeleted { get; set; }
  
        public int CreateBy { get; set; }
  
        public DateTime CreateDate { get; set; }
   
        public int? UpdateBy { get; set; }
    
        public DateTime? UpdateDate { get; set; }

    }
}
