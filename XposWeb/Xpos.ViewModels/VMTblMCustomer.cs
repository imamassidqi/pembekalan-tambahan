﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Xpos.ViewModels
{
    [Table("Tbl_M_Customer")]
    public partial class VMTblMCustomer
    {
      
        public int Id { get; set; }
    
        public string Name { get; set; } = null!;
    
        public string Email { get; set; } = null!;
     
        public string Password { get; set; } = null!;
     
        public string Address { get; set; } = null!;
    
        public string? Phone { get; set; }
   
        public int? RoleId { get; set; }
    
        public bool? IsDeleted { get; set; }
   
        public int CreateBy { get; set; }
    
        public DateTime CreateDate { get; set; }
       
        public int? UpdateBy { get; set; }
   
        public DateTime? UpdateDate { get; set; }
    }
}
