﻿using Microsoft.AspNetCore.Mvc;
using Xpos.DataModels;
using Xpos.ViewModels;
using XposWeb.api.DataAccess;

namespace XposWeb.api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CustomersController : Controller
    {
        private DACustomers customers;
        public CustomersController(XsisContext _db)
        {
            customers = new DACustomers(_db);
        }

        [HttpGet("[action]/{id?}")]
        public IActionResult Get(int id)
        {
            try
            {
                VMTblMCustomer? data = customers.GetById(id);
                if (data != null)
                {
                    return Ok(data);
                }
                else
                {
                    throw new Exception($"customer with ID ={id} does not exist!");
                }

            }

            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]

        public IActionResult GetAll()
        {
            try
            {
                List<VMTblMCustomer>? data = customers.GetAll();
                if (data!.Count > 0)
                {
                    return Ok(data);
                }
                else
                {
                    return BadRequest("Data not found");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("[action]/{filter?}")]

        public IActionResult GetBy(string filter)
        {
            try
            {
                if (!string.IsNullOrEmpty(filter))
                {
                    List<VMTblMCustomer>? data = customers.GetByFilter(filter);
                    if (data!.Count > 0)
                    {
                        return Ok(data);
                    }
                    else
                    {
                        throw new Exception($"Customer with Name or Email or Address or Password {filter} does not exist");
                    }
                }

                else
                    throw new ArgumentException("Please provide parts of Customer Name first!");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Customer Api" + ex.Message);
                return BadRequest(ex.Message);
            }
        }


        [HttpPost]

        public IActionResult Create(VMTblMCustomer customer)
        {
            try
            {
                return Created("/api/Categories", customers.CreateUpdate(customer));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Customer Api" + ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public IActionResult Update(VMTblMCustomer customer)
        {
            try
            {
                return Ok(customers.CreateUpdate(customer));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Customer Api" + ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        public IActionResult Delete(int id, int userId)
        {
            try
            {
                //get existing data
                VMTblMCustomer? existingCustomer = customers.GetById(id);
                existingCustomer!.IsDeleted = true;
                existingCustomer.UpdateBy = userId;

                return Ok(customers.CreateUpdate(existingCustomer));
            }
            catch (Exception ex)
            {
                //logging
                Console.WriteLine("Customer API : " + ex.Message);
                return BadRequest(ex.Message);
            }
        }
    }
}
