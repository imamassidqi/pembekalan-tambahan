﻿using Microsoft.AspNetCore.Mvc;
using Xpos.DataModels;
using Xpos.ViewModels;
using XposWeb.api.DataAccess;

namespace XposWeb.api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CategoriesController : Controller
    {
        private DACategories categories;
        public CategoriesController(XsisContext _db) {
            categories = new DACategories(_db);
        }
        //public IActionResult Index()
        //{
        //    return View();
        //}

        [HttpGet("[action]/{id?}")]
        public IActionResult Get(int id) 
        {
            try
            {
                VMTblMCategory? data = categories.GetById(id);
                if (data != null) {
                    return Ok(data);
                }
                else
                {
                    throw new Exception($"Category with ID ={id} does not exist!");
                }
            
            }

            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]

        public IActionResult GetAll() {
            try {
                List<VMTblMCategory>? data = categories.GetAll();
                if (data !.Count > 0)
                { 
                    return Ok(data);
                }
                else
                {
                    return BadRequest("Data not found");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("[action]/{filter?}")]

        public IActionResult GetBy(string filter)
        {
            try
            {
                if (!string.IsNullOrEmpty(filter))
                {
                    List<VMTblMCategory>? data = categories.GetByFilter(filter);
                    if (data!.Count > 0)
                    { 
                        return Ok(data);
                    }
                    else
                    {
                        throw new Exception($"Category with Name or Description {filter} does not exist");
                    }
                }

                else
                    throw new ArgumentException("Please provide parts of Category Name or Description first!");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Category Api"+ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]

        public IActionResult Create (VMTblMCategory category)
        {
            try
            {
                return Created("/api/Categories",categories.CreateUpdate(category));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Category Api" + ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public IActionResult Update (VMTblMCategory category)
        {
            try
            {
                return Ok(categories.CreateUpdate(category));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Category Api" + ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        public IActionResult Delete(int id, int userId)
        {
            try
            {
                //get existing data
                VMTblMCategory? existingCategory = categories.GetById(id);
                existingCategory!.IsDeleted = true;
                existingCategory.UpdateBy = userId;

                return Ok(categories.CreateUpdate(existingCategory));
            }
            catch (Exception ex)
            {
                //logging
                Console.WriteLine("Category API : " + ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("[action]/{pageIndex?}/{pageSize?}")]
        public IActionResult GetByPaging(int pageIndex, int pageSize)
        {
            try
            {
                List<VMTblMCategory>? data = categories.GetAll();
                if (data.Count() > 0)
                {
                    List<object> result = new List<object>();
                    result.Add(data.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList());
                    result.Add(data.Count());
                    return Ok(result);

                }
                else
                {
                    throw new Exception("Product has no data");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Category API: " + ex.Message);
                return BadRequest(ex.Message);
            }
        }
    }
}
