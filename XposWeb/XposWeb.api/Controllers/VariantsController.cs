﻿using Microsoft.AspNetCore.Mvc;
using Xpos.DataModels;
using Xpos.ViewModels;
using XposWeb.api.DataAccess;

namespace XposWeb.api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VariantsController : Controller
    {
        private DAVariants variants;

        public VariantsController(XsisContext _db) {
            variants = new DAVariants(_db);
        }

        [HttpGet]

        public IActionResult GetAll() {
        try
            {
                List<VMTblMVariant>? data = variants.GetAll();

                if (data.Count > 0)
                    return Ok(data);
                else
                {
                    throw new ArgumentNullException("Variant table kosong");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("[action]/{id?}")]
        public IActionResult Get(int id)
        {
            try
            {
                VMTblMVariant? data = variants.GetById(id);
                if (data != null)
                {
                    return Ok(data);
                }
                else
                {
                    throw new Exception($"Variant with ID ={id} does not exist!");
                }

            }

            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet("[action]/{filter?}")]
        public IActionResult GetBy(string filter)
        {
            try
            {
                if (!string.IsNullOrEmpty(filter))
                {
                    List<VMTblMVariant>? data = variants.GetByFilter(filter);
                    if (data!.Count > 0)
                    {
                        return Ok(data);
                    }
                    else
                    {
                        throw new Exception($"Variants with Name or Description or Category Name {filter} does not exist");
                    }
                }

                else
                    throw new ArgumentException("Please provide parts of Category Name or Description first!");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Variant Api" + ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]

        public IActionResult Create(VMTblMVariant variant)
        {
            try
            {
                return Created("/api/Variants", variants.CreateUpdate(variant));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Variant Api" + ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public IActionResult Update(VMTblMVariant variant)
        {
            try
            {
                return Ok(variants.CreateUpdate(variant));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Category Api" + ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        public IActionResult Delete(int id, int userId)
        {
            try
            {
                //get existing data
                VMTblMVariant? existingCategory = variants.GetById(id);
                existingCategory!.IsDeleted = true;
                existingCategory.UpdateBy = userId;

                return Ok(variants.CreateUpdate(existingCategory));
            }
            catch (Exception ex)
            {
                //logging
                Console.WriteLine("Variant API : " + ex.Message);
                return BadRequest(ex.Message);
            }
        }
    }
}
