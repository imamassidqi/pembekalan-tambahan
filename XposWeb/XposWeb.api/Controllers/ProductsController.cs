﻿using Microsoft.AspNetCore.Mvc;
using Xpos.DataModels;
using Xpos.ViewModels;
using XposWeb.api.DataAccess;

namespace XposWeb.api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductsController : Controller
    {
        private DAProducts products;

        public ProductsController(XsisContext _db)
        {
            products = new DAProducts(_db);
        }

        [HttpGet("[action]/{id?}")]
        public IActionResult Get(int id)
        {
            try
            {
                VMTblMProduct? data = products.GetById(id);
                if (data != null)
                {
                    return Ok(data);
                }
                else
                {
                    throw new Exception($"Product with ID ={id} does not exist!");
                }

            }

            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]

        public IActionResult GetAll()
        {
            try
            {
                List<VMTblMProduct>? data = products.GetAll();
                if (data!.Count > 0)
                {
                    return Ok(data);
                }
                else
                {
                    return BadRequest("Data not found");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("[action]/{filter?}")]

        public IActionResult GetBy(string filter)
        {
            try
            {
                if (!string.IsNullOrEmpty(filter))
                {
                    List<VMTblMProduct>? data = products.GetByFilter(filter);
                    if (data!.Count > 0)
                    {
                        return Ok(data);
                    }
                    else
                    {
                        throw new Exception($"Product with Name or VariantName or CategoryName {filter} does not exist");
                    }
                }

                else
                    throw new ArgumentException("Please provide parts of Product Name or VariantName or CategoryName first!");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Product Api" + ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]

        public IActionResult Create(VMTblMProduct category)
        {
            try
            {
                return Created("/api/Products", products.CreateUpdate(category));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Product Api" + ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public IActionResult Update(VMTblMProduct category)
        {
            try
            {
                return Ok(products.CreateUpdate(category));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Product Api" + ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        public IActionResult Delete(int id, int userId)
        {
            try
            {
                //get existing data
                VMTblMProduct? existingCategory = products.GetById(id);
                existingCategory!.IsDeleted = true;
                existingCategory.UpdateBy = userId;

                return Ok(products.CreateUpdate(existingCategory));
            }
            catch (Exception ex)
            {
                //logging
                Console.WriteLine("Product API : " + ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("[action]/{pageIndex?}/{pageSize?}")]
        public IActionResult GetByPaging(int pageIndex, int pageSize)
        {
            try
            {
                List<VMTblMProduct>? data = products.GetAll();

                if (data.Count() > 0)
                {
                    List<object>? result = new List<object>();
                    result.Add(data.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList());
                    result.Add(data.Count());
                    return Ok(result);
                }
                else
                {
                    throw new Exception("Product has no data");
                }
            }
            catch (Exception ex)
            {
                //logging
                Console.WriteLine("Product API : " + ex.Message);
                return BadRequest(ex.Message);
            }
        }
    }
}
