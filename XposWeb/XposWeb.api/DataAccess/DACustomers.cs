﻿using Xpos.DataModels;
using Xpos.ViewModels;

namespace XposWeb.api.DataAccess
{
    public class DACustomers
    {
        private readonly XsisContext db;
        public DACustomers(XsisContext _db)
        {
            db = _db;
        }

        public VMTblMCustomer? GetById(int id)
        {
            try
            {
                VMTblMCustomer? data = (
                    from c in db.TblMCustomers
                    where c.Id == id && c.IsDeleted == false
                    select new VMTblMCustomer
                    {
                        Id = c.Id,
                        Name = c.Name,
                        Email = c.Email,
                        Address = c.Address,
                        Password = c.Password,
                        Phone = c.Phone,
                        RoleId = c.RoleId,
                        IsDeleted = c.IsDeleted,

                        CreateBy = c.CreateBy,
                        CreateDate = c.CreateDate,
                        UpdateBy = c.UpdateBy,
                        UpdateDate = c.UpdateDate,
                    }
                    ).FirstOrDefault();
                return data;

            }
            catch (Exception ex)
            {
                //logging
                Console.WriteLine("Customer Error :" + ex.Message);
                return null;
            }
        }

        public List<VMTblMCustomer>? GetByFilter(string filter)
        {
            try
            {
                List<VMTblMCustomer>? data = (
                       from c in db.TblMCustomers
                       where c.IsDeleted == false
                       && (c.Name.Contains(filter) || c.Address.Contains(filter) || c.Email.Contains(filter)
                       || c.Phone.Contains(filter) || c.Password.Contains(filter))
                       select new VMTblMCustomer
                       {
                           Id = c.Id,
                           Name = c.Name,
                           Email = c.Email,
                           Address = c.Address,
                           Password = c.Password,
                           Phone = c.Phone,
                           RoleId = c.RoleId,
                           IsDeleted = c.IsDeleted,

                           CreateBy = c.CreateBy,
                           CreateDate = c.CreateDate,
                           UpdateBy = c.UpdateBy,
                           UpdateDate = c.UpdateDate,
                       }
                    ).ToList();
                return data;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Customer Error :" + ex.Message);
                return new List<VMTblMCustomer>();
            }
        }

        public List<VMTblMCustomer> GetAll()
        {
            return GetByFilter(string.Empty);

        }

        public TblMCustomer CreateUpdate(VMTblMCustomer input)
        {
            TblMCustomer newData = new TblMCustomer();
            newData.Name = input.Name;
            newData.Address = input.Address;
            newData.Password = input.Password;
            newData.Phone = input.Phone;
            newData.RoleId = input.RoleId;
            newData.Email = input.Email;
            newData.IsDeleted = (input.IsDeleted ?? false);

            //create new category
            if (input.Id == 0 || input.Id == null)
            {
                newData.CreateBy = input.CreateBy;
                // newData.CreateDate = DateTime.Now;
                db.Add(newData);
            }

            //update existing category
            else
            {
                //check existing data
                VMTblMCustomer? existingCategory = GetById(input.Id);
                if (existingCategory != null)
                {
                    newData.Id = input.Id;
                    newData.CreateBy = existingCategory.CreateBy;
                    newData.CreateDate = existingCategory.CreateDate;
                    newData.UpdateBy = input.UpdateBy;
                    newData.UpdateDate = DateTime.Now;
                    db.Update(newData);
                }
                else
                {
                    throw new Exception($"Category with ID = {input.Id} does not exist");
                }
            }

            //execute query
            db.SaveChanges();

            return newData;
        }
    }
}
