﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Storage;
using Xpos.DataModels;
using Xpos.ViewModels;

namespace XposWeb.api.DataAccess
{
    public class DACategories
    {
        private readonly XsisContext db;
        public DACategories(XsisContext _db)
        {
            db = _db;
        }

        public VMTblMCategory? GetById(int id)
        {
            try
            {
                VMTblMCategory? data = (
                    from c in db.TblMCategories
                    where c.Id == id && c.IsDeleted == false
                    select new VMTblMCategory
                    {
                        Id = c.Id,
                        CategoryName = c.CategoryName,
                        Description = c.Description,
                        IsDeleted = c.IsDeleted,

                        CreateBy = c.CreateBy,
                        CreateDate = c.CreateDate,
                        UpdateBy = c.UpdateBy,
                        UpdateDate = c.UpdateDate, 
                    }
                    ).FirstOrDefault();
                return data;
            
            }
            catch (Exception ex){ 
                //logging
                Console.WriteLine("Category Error :"+ex.Message);
                return null;
            }
        }

        public List<VMTblMCategory> GetAll()
        {
            return GetByFilter("");
            //try
            //{
            //    List<VMTblMCategory>? data = (
            //           from c in db.TblMCategories
            //           where c.IsDeleted == false
            //           select new VMTblMCategory
            //           {
            //               Id = c.Id,
            //               CategoryName = c.CategoryName,
            //               Description = c.Description,
            //               IsDeleted = c.IsDeleted,

            //               CreateBy = c.CreateBy,
            //               CreateDate = c.CreateDate,
            //               UpdateBy = c.UpdateBy,
            //               UpdateDate = c.UpdateDate,
            //           }
            //        ).ToList();
            //    return data;
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine("Category Error :" + ex.Message);
            //    return new List<VMTblMCategory>();
            //}

        }

        public List<VMTblMCategory>? GetByFilter(string filter)
        {
            try
            {
                List<VMTblMCategory>? data = (
                       from c in db.TblMCategories
                       where c.IsDeleted == false
                       && (c.CategoryName.Contains(filter)||c.Description.Contains(filter))
                       select new VMTblMCategory
                       {
                           Id = c.Id,
                           CategoryName = c.CategoryName,
                           Description = c.Description,
                           IsDeleted = c.IsDeleted,

                           CreateBy = c.CreateBy,
                           CreateDate = c.CreateDate,
                           UpdateBy = c.UpdateBy,
                           UpdateDate = c.UpdateDate,
                       }
                    ).ToList();
                return data;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Category Error :" + ex.Message);
                return new List<VMTblMCategory>();
            }
        }

        public TblMCategory CreateUpdate (VMTblMCategory input)
        {
                TblMCategory newData = new TblMCategory();
                newData.CategoryName = input.CategoryName;
                newData.Description = input.Description;
                newData.IsDeleted = (input.IsDeleted ?? false);
            //proses DB Transaction
            using (IDbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {

                //create new category
                if (input.Id == 0 || input.Id == null)
                {
                    newData.CreateBy = input.CreateBy;
                    // newData.CreateDate = DateTime.Now;
                    db.Add(newData);
                }

                //update existing category
                else
                {
                    //check existing data
                    VMTblMCategory? existingCategory = GetById(input.Id);
                    if (existingCategory != null)
                    {
                        newData.Id = input.Id;
                        newData.CreateBy = existingCategory.CreateBy;
                        newData.CreateDate = existingCategory.CreateDate;
                        newData.UpdateBy = input.UpdateBy;
                        newData.UpdateDate = DateTime.Now;
                        db.Update(newData);
                    }
                    else
                    {
                        throw new Exception($"Category with ID = {input.Id} does not exist");
                    }
                }

                //execute query
                db.SaveChanges();

                dbTran.Commit();

                }
                catch (Exception ex)
                {
                    dbTran.Rollback();

                    Console.WriteLine(ex.Message);
                }

            }
            return newData;
        }

    }
}
