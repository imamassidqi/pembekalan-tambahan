﻿using Microsoft.EntityFrameworkCore.Storage;
using Xpos.DataModels;
using Xpos.ViewModels;

namespace XposWeb.api.DataAccess
{
    public class DAProducts
    {
        private readonly XsisContext db;
      

        public DAProducts(XsisContext _db)
        {
            db = _db;
        }

    
        public List<VMTblMProduct>? GetAll()
        {
            return (
                from p in db.TblMProducts
                join v in db.TblMVariants
                on p.VariantId equals v.Id
                join c in db.TblMCategories
                on v.CategoryId equals c.Id
                where p.IsDeleted == false
                select new VMTblMProduct
                {
                    Id = p.Id,
                    Name = p.Name,
                    Stock = p.Stock,
                    Image = p.Image,
                    Price = p.Price,

                    VariantId = p.VariantId,
                    VariantName = v.Name,
                    CategoryName = c.CategoryName,
                    CategoryId = v.CategoryId,

                    IsDeleted = p.IsDeleted,
                    CreateBy = p.CreateBy,
                    CreateDate = p.CreateDate,

                    UpdateBy = p.UpdateBy,
                    UpdateDate = p.UpdateDate,
                }
                ).ToList();
        }

        public VMTblMProduct? GetById(int id)
        {
            try
            {
                VMTblMProduct? data = (
                     from p in db.TblMProducts
                     join v in db.TblMVariants
                     on p.VariantId equals v.Id
                     join c in db.TblMCategories
                     on v.CategoryId equals c.Id
                     where p.Id == id && p.IsDeleted == false
                    select new VMTblMProduct
                    {
                        Id = p.Id,
                        Name = p.Name,
                        Stock = p.Stock,
                        Image = p.Image,
                        Price = p.Price,

                        VariantId = p.VariantId,
                        VariantName = v.Name,
                        CategoryName = c.CategoryName,
                        CategoryId = v.CategoryId,

                        IsDeleted = p.IsDeleted,
                        CreateBy = p.CreateBy,
                        CreateDate = p.CreateDate,

                        UpdateBy = p.UpdateBy,
                        UpdateDate = p.UpdateDate,
                    }
                    ).FirstOrDefault();
                return data;

            }
            catch (Exception ex)
            {
                //logging
                Console.WriteLine("Product Error :" + ex.Message);
                return null;
            }
        }

        public List<VMTblMProduct>? GetByFilter(string filter)
        {
            try
            {
                List<VMTblMProduct>? data = (
                       from p in db.TblMProducts
                       join v in db.TblMVariants
                       on p.VariantId equals v.Id
                       join c in db.TblMCategories
                       on v.CategoryId equals c.Id
                       where p.IsDeleted == false
                       && (p.Name.Contains(filter) || c.CategoryName.Contains(filter) || v.Name.Contains(filter))
                       select new VMTblMProduct
                       {
                           Id = p.Id,
                           Name = p.Name,
                           Stock = p.Stock,
                           Image = p.Image,
                           Price = p.Price,

                           VariantId = p.VariantId,
                           VariantName = v.Name,
                           CategoryName = c.CategoryName,
                           CategoryId = v.CategoryId,

                           IsDeleted = p.IsDeleted,
                           CreateBy = p.CreateBy,
                           CreateDate = p.CreateDate,

                           UpdateBy = p.UpdateBy,
                           UpdateDate = p.UpdateDate,
                       }
                    ).ToList();
                return data;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Product Error :" + ex.Message);
                return new List<VMTblMProduct>();
            }
        }

        public TblMProduct CreateUpdate(VMTblMProduct input)
        {
            TblMProduct newData = new TblMProduct();
            newData.Name = input.Name;
            newData.Price = input.Price;
            newData.Image = input.Image;
            newData.VariantId = input.VariantId;
            newData.Stock = input.Stock;
            newData.IsDeleted = (input.IsDeleted ?? false);
            //proses DB Transaction
            using (IDbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {

                    //create new category
                    if (input.Id == 0 || input.Id == null)
                    {
                        newData.CreateBy = input.CreateBy;
                        newData.CreateDate = DateTime.Now;
                        db.Add(newData);
                    }

                    //update existing category
                    else
                    {
                        //check existing data
                        VMTblMProduct? existingCategory = GetById(input.Id);
                        if (existingCategory != null)
                        {
                            newData.Id = existingCategory.Id;
                            newData.CreateBy = existingCategory.CreateBy;
                            newData.CreateDate = existingCategory.CreateDate;

                            newData.UpdateBy = input.UpdateBy;
                            newData.UpdateDate = DateTime.Now;
                            db.Update(newData);
                        }
                        else
                        {
                            throw new Exception($"Product with ID = {input.Id} does not exist");
                        }
                    }

                    //execute query
                    db.SaveChanges();

                    dbTran.Commit();

                }
                catch (Exception ex)
                {
                    dbTran.Rollback();

                    Console.WriteLine(ex.Message);
                }

            }
            return newData;
        }


    }
}
