﻿using Microsoft.EntityFrameworkCore.Storage;
using Xpos.DataModels;
using Xpos.ViewModels;

namespace XposWeb.api.DataAccess
{
    public class DAVariants
    {
        private readonly XsisContext db;

        public DAVariants(XsisContext _db) 
        {
            db = _db;
        }

        public List<VMTblMVariant>? GetAll()
        {
            return (
                from v in db.TblMVariants
                join c in db.TblMCategories
                on v.CategoryId equals c.Id
                where v.IsDeleted == false
                select new VMTblMVariant
                {
                    Id = v.Id,
                    Name = v.Name,
                    Description = v.Description,

                    CategoryName = c.CategoryName,
                    CategoryId = c.Id, 

                    IsDeleted = v.IsDeleted,
                    CreateBy = v.CreateBy,
                    CreateDate = v.CreateDate,

                    UpdateBy = v.UpdateBy,
                    UpdateDate  = v.UpdateDate,
                }
                ).ToList();
        }

        public VMTblMVariant? GetById(int id)
        {
            try
            {
                VMTblMVariant? data = (
                    from v in db.TblMVariants
                    join c in db.TblMCategories
                    on v.CategoryId equals c.Id
                    where v.Id == id && v.IsDeleted == false
                    select new VMTblMVariant
                    {
                        Id = v.Id,
                        Name= v.Name,
                        CategoryId = v.CategoryId,
                        CategoryName= c.CategoryName,
                        Description = v.Description,
                        IsDeleted = v.IsDeleted,

                        CreateBy = v.CreateBy,
                        CreateDate = v.CreateDate,
                        UpdateBy = v.UpdateBy,
                        UpdateDate = v.UpdateDate,
                    }
                    ).FirstOrDefault();
                return data;

            }
            catch (Exception ex)
            {
                //logging
                Console.WriteLine("Variant Error :" + ex.Message);
                return null;
            }
        }

        public List<VMTblMVariant>? GetByFilter(string filter)
        {
            try
            {
                List<VMTblMVariant>? data = (
                       from v in db.TblMVariants
                       join c in db.TblMCategories
                       on v.CategoryId equals c.Id
                       where v.IsDeleted == false
                       && (v.Name.Contains(filter) || c.CategoryName.Contains(filter) || v.Description.Contains(filter))
                       select new VMTblMVariant
                       {
                           Id = v.Id,
                           Name = v.Name,
                           Description = v.Description,

                           CategoryName = c.CategoryName,
                           CategoryId = c.Id,

                           IsDeleted = v.IsDeleted,
                           CreateBy = v.CreateBy,
                           CreateDate = v.CreateDate,

                           UpdateBy = v.UpdateBy,
                           UpdateDate = v.UpdateDate,
                       }
                    ).ToList();
                return data;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Category Error :" + ex.Message);
                return new List<VMTblMVariant>();
            }
        }
        public TblMVariant CreateUpdate(VMTblMVariant input)
        {
            TblMVariant newData = new TblMVariant();
            newData.Name = input.Name;
            newData.Description = input.Description;
            newData.CategoryId = input.CategoryId;           
            newData.IsDeleted = (input.IsDeleted ?? false);
            //proses DB Transaction
            using (IDbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {

                    //create new category
                    if (input.Id == 0 || input.Id == null)
                    {
                        newData.CreateBy = input.CreateBy;
                        // newData.CreateDate = DateTime.Now;
                        db.Add(newData);
                    }

                    //update existing category
                    else
                    {
                        //check existing data
                        VMTblMVariant? existingCategory = GetById(input.Id);
                        if (existingCategory != null)
                        {
                            newData.Id = input.Id;
                            newData.CreateBy = existingCategory.CreateBy;
                            newData.CreateDate = existingCategory.CreateDate;
                            newData.UpdateBy = input.UpdateBy;
                            newData.UpdateDate = DateTime.Now;
                            db.Update(newData);
                        }
                        else
                        {
                            throw new Exception($"Category with ID = {input.Id} does not exist");
                        }
                    }

                    //execute query
                    db.SaveChanges();

                    dbTran.Commit();

                }
                catch (Exception ex)
                {
                    dbTran.Rollback();

                    Console.WriteLine(ex.Message);
                }

            }
            return newData;
        }
    }
}
