﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Xpos.DataModels;
using Xpos.ViewModels;
using XposWeb.api.DataAccess;
using XposWeb.api.DataModels;
using XposWeb.api.Transaction.DataAccess;


namespace XposWeb.api.Transaction.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : Controller
    {
        private readonly DAOrders orders;

        public OrdersController(XsisContext _db)
        {
            orders = new DAOrders(_db);
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            try
            {
                List<VMTblOrder>? data = orders.GetAllHeader();
                if (data!.Count > 0)
                {
                    return Ok(data);
                }
                else
                {
                    return BadRequest("Data not found");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet("[action]/{filter?}")]

        public IActionResult GetByFilter(string filter)
        {
            try
            {
                if (!string.IsNullOrEmpty(filter))
                {
                    List<VMTblOrder>? data = orders.GetByFilter(filter);
                    if (data!.Count > 0)
                    {
                        return Ok(data);
                    }
                    else
                    {
                        throw new Exception($"Order with Name or Description or Category Name {filter} does not exist");
                    }
                }

                else
                    throw new ArgumentException("Please provide parts of Category Name or Description first!");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Order Api" + ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("[action]/{id?}")]
        public IActionResult Get(int id)
        {
            try
            {
                VMTblOrder? data = orders.GetById(id);
                if (data != null)
                {
                    return Ok(data);
                }
                else
                {
                    throw new Exception($"Orders with ID ={id} does not exist!");
                }

            }

            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("[action]/{filter?}")]

        public IActionResult GetOrderDetailByFilter(string filter)
        {
            try
            {
                if (!string.IsNullOrEmpty(filter))
                {
                    List<VMTblTOrderDetail>? data = orders.GetOrderDetailByFilter(filter);
                    if (data!.Count > 0)
                    {
                        return Ok(data);
                    }
                    else
                    {
                        throw new Exception($"Order with Name or Description {filter} does not exist");
                    }
                }

                else
                    throw new ArgumentException("Please provide parts of Category Name or Description first!");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Order Api" + ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]

        public IActionResult Create(VMTblOrder order)
        {
            try
            {
                return Created("/api/Orders", orders.CreateUpdate(order));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Order Api" + ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public IActionResult Update(VMTblOrder order)
        {
            try
            {
                return Ok(orders.CreateUpdate(order));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Order Api" + ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        public IActionResult Delete(int id, int userId)
        {
            try
            {
                //get existing data
                VMTblOrder? existingOrder = orders.GetById(id);
                existingOrder!.IsDeleted = true;
                existingOrder.UpdateBy = userId;

                return Ok(orders.CreateUpdate(existingOrder));
            }
            catch (Exception ex)
            {
                //logging
                Console.WriteLine("order API : " + ex.Message);
                return BadRequest(ex.Message);
            }
        }
    }
}
