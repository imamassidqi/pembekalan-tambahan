﻿using Microsoft.EntityFrameworkCore.Storage;
using Xpos.DataModels;
using Xpos.ViewModels;

namespace XposWeb.api.Transaction.DataAccess
{
    public class DAOrders
    {
        private readonly XsisContext db;
    

        public DAOrders(XsisContext _db)
        {
            db = _db;
        
        }
        private string TrsCodeGenerator(int? lastId)
        {
            string trsNo = (lastId != null || lastId > 0) ? lastId++.ToString().PadLeft(5, '0')
                : "00001";
            return $"XA-{DateTime.Now.ToString("yyyyMMdd")}-{trsNo}";
        }
        public List<VMTblOrder> GetAllHeader()
        {
            return (
                from oh in db.TblTOrderHeaders
                join c in db.TblMCustomers on oh.CustomerId equals c.Id
                where oh.IsDeleted == false
                select new VMTblOrder
                {
                    Id = oh.Id,
                    TrxCode = oh.TrxCode,
                    TotalQty = oh.TotalQty,
                    Amount = oh.Amount,
                    IsCheckout = oh.IsCheckout,
                    IsDeleted = oh.IsDeleted,
                    CustomerId = oh.CustomerId,
                    CustomerName = c.Name,
                    CreateBy = oh.CreateBy,
                    CreateDate = oh.CreateDate,
                    UpdateBy = oh.UpdateBy,
                    UpdateDate = oh.UpdateDate,

                    TblTOrderDetails = 
                    (
                    from od in db.TblTOrderDetails 
                    join p in db.TblMProducts on od.ProductId equals p.Id
                    where od.OrderHeaderId == oh.Id && od.IsDeleted == false
                    select new VMTblTOrderDetail
                    {
                        Id = od.Id,
                        Qty = od.Qty,
                        Price = od.Price,
                        OrderHeaderId = od.OrderHeaderId,
                        CreateBy = od.CreateBy,
                        CreateDate = od.CreateDate,
                        IsDeleted = od.IsDeleted,
                        ProductName = p.Name,
                        ProductId = od.ProductId,
                        UpdateBy = od.UpdateBy,
                        UpdateDate = od.UpdateDate,
                    }
                    ).ToList()
                    
                }
                ).ToList();
        }

        public List<VMTblOrder>? GetByFilter (string filter)
        {
            return (
                from oh in db.TblTOrderHeaders
                join c in db.TblMCustomers on oh.CustomerId equals c.Id
                where oh.IsDeleted == false
                && (c.Name.Contains(filter) || oh.TrxCode.Contains(filter))
                select new VMTblOrder
                {
                    Id = oh.Id,
                    TrxCode = oh.TrxCode,
                    TotalQty = oh.TotalQty,
                    Amount = oh.Amount,
                    IsCheckout = oh.IsCheckout,
                    IsDeleted = oh.IsDeleted,
                    CustomerId = oh.CustomerId,
                    CustomerName = c.Name,
                    CreateBy = oh.CreateBy,
                    CreateDate = oh.CreateDate,
                    UpdateBy = oh.UpdateBy,
                    UpdateDate = oh.UpdateDate,

                    TblTOrderDetails =
                    (
                    from od in db.TblTOrderDetails
                    join p in db.TblMProducts on od.ProductId equals p.Id
                    where od.OrderHeaderId == oh.Id && od.IsDeleted == false
                    select new VMTblTOrderDetail
                    {
                        Id = od.Id,
                        Qty = od.Qty,
                        Price = od.Price,
                        CreateBy = od.CreateBy,
                        CreateDate = od.CreateDate,
                        IsDeleted = od.IsDeleted,
                        ProductName = p.Name,
                        ProductId = od.ProductId,
                        UpdateBy = od.UpdateBy,
                        UpdateDate = od.UpdateDate,
                    }
                    ).ToList()

                }
                ).ToList();
        }

        public VMTblOrder? GetById(int id)
        {
            try
            {
                VMTblOrder? data = (
             from oh in db.TblTOrderHeaders
             join c in db.TblMCustomers on oh.CustomerId equals c.Id
             where oh.Id == id && oh.IsDeleted == false
             select new VMTblOrder
             {
                 Id = oh.Id,
                 TrxCode = oh.TrxCode,
                 TotalQty = oh.TotalQty,
                 Amount = oh.Amount,
                 IsCheckout = oh.IsCheckout,
                 IsDeleted = oh.IsDeleted,
                 CustomerId = oh.CustomerId,
                 CustomerName = c.Name,
                 CreateBy = oh.CreateBy,
                 CreateDate = oh.CreateDate,
                 UpdateBy = oh.UpdateBy,
                 UpdateDate = oh.UpdateDate,

                 TblTOrderDetails =
                    (
                    from od in db.TblTOrderDetails
                    join p in db.TblMProducts on od.ProductId equals p.Id
                    where od.OrderHeaderId == oh.Id && od.IsDeleted == false
                    select new VMTblTOrderDetail
                    {
                        Id = od.Id,
                        Qty = od.Qty,
                        Price = od.Price,
                        OrderHeaderId = od.OrderHeaderId,
                        CreateBy = od.CreateBy,
                        CreateDate = od.CreateDate,
                        IsDeleted = od.IsDeleted,
                        ProductName = p.Name,
                        ProductId = od.ProductId,
                        UpdateBy = od.UpdateBy,
                        UpdateDate = od.UpdateDate,
                    }
                    ).ToList()

             }
             ).FirstOrDefault();
                return data;
            }
            catch ( Exception ex ) 
            {
                Console.WriteLine("Order Error :" + ex.Message);
                return null;
            }
        }

   
        public List<VMTblTOrderDetail>? GetOrderDetailByFilter(string filter)
        {
            try
            {
                List<VMTblTOrderDetail>? data = (
                       from a in db.TblTOrderDetails
                       join b in db.TblMProducts on a.ProductId equals b.Id
                       where a.IsDeleted == false
                       && (b.Name.Contains(filter))
                       select new VMTblTOrderDetail
                       {
                           Id = a.Id,
                           Qty = a.Qty,
                           Price = a.Price,
                           ProductId= a.ProductId,
                           OrderHeaderId = a.OrderHeaderId,
                           ProductName = b.Name,
                           CreateBy = a.CreateBy,
                           CreateDate = a.CreateDate,
                           IsDeleted = a.IsDeleted,
                           UpdateBy = a.UpdateBy,
                           UpdateDate = a.UpdateDate,
                       }
                    ).ToList();
                return data;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Order Error :" + ex.Message);
                return new List<VMTblTOrderDetail>();
            }
        }

        public VMTblOrder CreateUpdate(VMTblOrder input)
        {
            int newOrderHeaderId = 0;
            //proses DB Transaction
            using (IDbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                TblTOrderHeader? orderHeader = new TblTOrderHeader();
                TblTOrderDetail? orderDetail = new TblTOrderDetail();
                TblMProduct? product = new TblMProduct();

                try
                {

                    //create new category
                    if (input.Id == 0 || input.Id == null)
                    {
                        orderHeader.Amount = input.Amount;
                        orderHeader.TotalQty = input.TotalQty;
                        orderHeader.CustomerId = input.CustomerId;
                        orderHeader.IsCheckout = input.IsCheckout;
                        orderHeader.IsDeleted = input.IsDeleted ?? false;

                        orderHeader.TrxCode = TrsCodeGenerator(
                        db.TblTOrderHeaders.OrderBy(oh => oh.Id).Select(oh => oh.Id).LastOrDefault());
                        orderHeader.CreateBy = input.CreateBy;

                        db.Add(orderHeader);
                        db.SaveChanges();
                        newOrderHeaderId = orderHeader.Id;

                        //order details
                        foreach (VMTblTOrderDetail item in input.TblTOrderDetails)
                        {
                            orderDetail = new TblTOrderDetail();
                            orderDetail.OrderHeaderId = orderHeader.Id;
                            orderDetail.ProductId = item.ProductId;
                            orderDetail.Qty = item.Qty;
                            orderDetail.Price = item.Price;
                            orderDetail.IsDeleted = item.IsDeleted ?? false;
                            orderDetail.CreateBy = orderHeader.CreateBy;
                            orderDetail.CreateDate = orderHeader.CreateDate;

                            db.Add(orderDetail);
                            db.SaveChanges();

                            //update product stock
                            product = new TblMProduct();
                            product = db.TblMProducts.Find(item.ProductId);
                            if (product != null)
                            {
                                //check if current stock is enough
                                if (product.Stock >= item.Qty)
                                {
                                    product.Stock -= item.Qty;

                                    db.Update(product);
                                    db.SaveChanges();
                                }
                                else 
                                {
                                    throw new Exception($"Product {product.Name} with ID ={product.Id} does not have enough stock");
                                }
                            }
                            else
                            {
                                throw new Exception($"Product with ID ={product.Id} does not exist");

                            }
                        }
                    }

                    else
                    {
                    //update existing order
                        VMTblOrder? existingOrder = GetById(input.Id);
                        if (existingOrder != null)
                        {
                            orderHeader.Id = existingOrder.Id;
                            orderHeader.CustomerId = existingOrder.CustomerId;
                            orderHeader.TrxCode = existingOrder.TrxCode;
                            orderHeader.CreateBy = existingOrder.CreateBy;
                            orderHeader.CreateDate = existingOrder.CreateDate;

                            orderHeader.IsCheckout = input.IsCheckout;
                            orderHeader.Amount = input.Amount;
                            orderHeader.TotalQty = input.TotalQty;
                            orderHeader.UpdateBy = input.UpdateBy;
                            orderHeader.UpdateDate = DateTime.Now;
                            orderHeader.IsDeleted = false;

                            db.Update(orderHeader);
                            db.SaveChanges();

                            //process each order detail
                            foreach(VMTblTOrderDetail item in input.TblTOrderDetails)
                            {
                                //get existing order detail
                                VMTblTOrderDetail existingItem = existingOrder.TblTOrderDetails.FirstOrDefault(x => x.Id == item.Id);
                                orderDetail = new TblTOrderDetail();
                                orderDetail.Id = existingItem.Id;
                                orderDetail.OrderHeaderId = existingItem.OrderHeaderId;
                                orderDetail.CreateBy = existingItem.CreateBy;
                                orderDetail.CreateDate = existingItem.CreateDate;
                                orderDetail.IsDeleted = false; 

                                orderDetail.ProductId = item.ProductId;
                                orderDetail.Price = item.Price;
                                orderDetail.Qty = item.Qty;

                                orderDetail.UpdateBy = orderHeader.UpdateBy;
                                orderDetail.UpdateDate =orderHeader.UpdateDate; 

                                db.Update(orderDetail);
                                db.SaveChanges();

                                //get existing product data

                                //update product stock
                                product = db.TblMProducts.Find(item.ProductId);
                                if (product != null)
                                {
                                    if(existingItem.Qty > item.Qty)
                                    {
                                        //if item Qty less than before
                                        product.Stock += (existingItem.Qty - item.Qty);
                                    }
                                    else
                                    {
                                        if(product.Stock >= (item.Qty - existingItem.Qty))
                                        {
                                            product.Stock -= (item.Qty - existingItem.Qty);
                                        }
                                        else
                                        {
                                            throw new Exception($"product {item.ProductName} with ID {item.Id} does not have enough stock!");
                                        }
                                    }

                                    db.Update(product); 
                                    db.SaveChanges();
                                }
                                else 
                                {
                                    //product does not exist
                                    throw new Exception($"product {item.ProductName} with ID {item.Id} does not have enough stock!");

                                }
                            }

                            newOrderHeaderId = orderHeader.Id;
                        }
                        else
                        {
                            throw new Exception($"Order with ID {input.Id} does not exist");
                        }
                    }
                    dbTran.Commit();
                }
                catch (Exception ex)
                {
                    dbTran.Rollback();

                    Console.WriteLine(ex.Message);

                    throw new Exception(ex.Message);
                }

            }
            return GetById(newOrderHeaderId);
        }


    }
}
